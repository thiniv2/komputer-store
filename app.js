const computerElement = document.getElementById("computers");
const priceElement = document.getElementById("price");
const addElement = document.getElementById("add");
const cartElement = document.getElementById("cart");
const quantityElement = document.getElementById("quantity");
const payButtonElement = document.getElementById("pay");
const totalDueElement = document.getElementById("totalDue");
const loanButtonElement = document.getElementById("loan");
const balanceElement = document.getElementById("totalBalance");
const totalLoanElement = document.getElementById("totalLoan");
const workButtonElement = document.getElementById("workButton");
const bankButtonElement = document.getElementById("bankButton");
const totalPayElement = document.getElementById("totalPay");
const repayLoanButtonElement = document.getElementById("repayLoan");
const specsElement = document.getElementById("specs");
const computerImgElement = document.getElementById("computerImage");
const descriptionELement = document.getElementById("description");

const baseUrlPath = "https://noroff-komputer-store-api.herokuapp.com/";
let computers = [];
let cart = [];
let totalDue = 0.0;
let totalBalance = 0.0;
let totalLoan = 0.0;
let totalPay = 0.0;

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
  .then((response) => response.json())
  .then((data) => (computers = data))
  .then((computers) => addComputerToMenu(computers))
  .catch(() => console.log("Error fetching data!"));

/*
  Appends computerElement with data got from json().
*/
const addComputerToMenu = (computers) => {
  computers.forEach((x) => addOneComputerToMenu(x));
  priceElement.innerText = `${computers[0].price.toFixed(2)} Kr`;
  specsElement.innerText = `${computers[0].specs}`;
  computerImgElement.src = `${baseUrlPath}${computers[0].image}`;
  descriptionELement.innerText = `${computers[0].description}`;
};

const addOneComputerToMenu = (computer) => {
  if (computer.active === true) {
    const oneComputerElement = document.createElement("option");
    oneComputerElement.value = computer.id;
    oneComputerElement.appendChild(document.createTextNode(computer.title));
    computerElement.appendChild(oneComputerElement);
  }
};

/*
  Increments totalPay value by 100 with each click
*/
const handleWorkButton = () => {
  totalPayElement.innerText = "pay: " + (totalPay += 100).toFixed(2) + " Kr";
};

/*
	Handles "bank" -button.
	if you have any loan it takes 10%
	of your totalPay and decrements it from your totalLoan
	and totalPay. If totalLoan goes to negative value it
	adds the overpayment to your total balance.
	If there's not any loan, adds your totalPay straight
	to totalBalance and resets totalPay.
*/
const handleBankingButton = () => {
  if (totalLoan > 0) {
    const charge = 0.1 * totalPay;
    totalLoan -= charge;
    totalPay -= charge;
    if (totalLoan <= 0) {
      totalBalance -= totalLoan;
      totalLoan = 0;
      repayLoanButtonElement.style.display = "none";
    }
  }
  totalBalance += totalPay;
  totalPay = 0;
  updateValues();
};

/*
	Handles "repay loan" -button
	when pressed decrements totalLoan by your totalPay.
	if totalLoan value would go to negative value, adds the remaining
	pay to your totalBalance then sets
	the "repay loan" -button invisible again.
*/
const handleRepayLoanButton = () => {
  totalLoan -= totalPay;
  if (totalLoan <= 0) {
    totalBalance -= totalLoan;
    totalLoan = 0;
    repayLoanButtonElement.style.display = "none";
  }
  totalPay = 0;
  updateValues();
};

/*
	Handles "take a loan" -button.
	prompts you how much you want to loan, adds
	the value to your totalBalance and also saves it
	to a variable called totalLoan.
	shows "repay loan" -button.
*/
const handleTakeLoan = () => {
  const loanAmount = prompt("Enter loan amount");
  if (
    parseFloat(loanAmount) > totalBalance * 2 ||
    isNaN(loanAmount) ||
    totalLoan > 0 ||
    !loanAmount
  )
    return alert("cannot take loan");
  totalBalance += parseFloat(loanAmount);
  totalLoan = parseFloat(loanAmount);
  repayLoanButtonElement.style.display = "block";
  updateValues();
};

/*
  Modifys selectedComputer image path, if it includes ".jpg"
  change it so ".png" and vice versa.
*/
const changeJpgPng = (selectedComputer) => {
  if (selectedComputer.image.includes(".jpg")) {
    computerImgElement.src =
      `${baseUrlPath}` + selectedComputer.image.replace(".jpg", ".png");
  } else if (selectedComputer.image.includes(".png"))
    computerImgElement.src =
      `${baseUrlPath}` + selectedComputer.image.replace(".png", ".jpg");
  else console.log("Image not found");
};

/*
  Returns TRUE or FALSE if image path is valid
*/
const UrlExists = (url, callback) => {
  fetch(url).then(function (status) {
    callback(status.ok);
  });
};

/*
  This function is called whenever new computer is selected.
  Updates selected computers description, specs and price accordingly.
*/
const handleComputerMenuChange = (e) => {
  const selectedComputer = computers[e.target.selectedIndex];
  const imageUrl = `${baseUrlPath}${selectedComputer.image}`;
  priceElement.innerText = `${selectedComputer.price.toFixed(2)} Kr`;
  descriptionELement.innerText = `${selectedComputer.description}`;
  UrlExists(imageUrl, function (exists) {
    if (exists) {
      computerImgElement.src = imageUrl;
    } else {
      changeJpgPng(selectedComputer);
    }
  });
};

/*
  Displays selectedComputer specs and changes them accordingly
*/
const showComputerSpecs = () => {
  const selectedComputer = computers[computerElement.selectedIndex];
  specsElement.innerText = `${selectedComputer.specs}`;
};

/*
  Handles the "add to cart" -button.
*/
const handleAddComputer = () => {
  const selectedComputer = computers[computerElement.selectedIndex];
  const quantity = parseInt(quantityElement.value);
  const cartItem = document.createElement("li");
  const lineTotal = quantity * selectedComputer.price;
  cartItem.innerText = `${selectedComputer.title}
  ${selectedComputer.price.toFixed(2)} Kr ${quantity}
  ${lineTotal.toFixed(2)} Kr`;
  cartElement.appendChild(cartItem);
  totalDue += lineTotal;
  updateValues();
};

/*
	handles "pay now" -button.
	checks for any child elements to see if cart is empty.
	compares your totalBalance and totalDue if totalBalance is 
	higher than	totalDue decrement totalBalance by totalDue.
*/
const handlePay = () => {
  if (cartElement.getElementsByTagName("li").length <= 0)
    return alert("Cart is empty!");
  else if (totalBalance >= totalDue) {
    balanceElement.innerText =
      "balance: " + (totalBalance -= totalDue).toFixed(2) + " Kr";
    alert("You are now a owner of a new computer!");
    clearCart();
  } else alert("Not enough money!");
};

/*
	clears the cart from any child elements.
*/
const clearCart = () => {
  cartElement.innerText = "";
  totalDue = 0;
  updateValues();
};

const updateValues = () => {
  totalDueElement.innerText = `Total Due: ${totalDue.toFixed(2)} Kr`;
  balanceElement.innerText = `balance: ${totalBalance.toFixed(2)} Kr`;
  totalLoanElement.innerText = `Total loan: ${totalLoan.toFixed(2)} Kr`;
  totalPayElement.innerText = `pay: ${totalPay.toFixed(2)} Kr`;
};

computerElement.addEventListener("change", handleComputerMenuChange);
computerElement.addEventListener("change", showComputerSpecs);
addElement.addEventListener("click", handleAddComputer);
payButtonElement.addEventListener("click", handlePay);
loanButtonElement.addEventListener("click", handleTakeLoan);
workButtonElement.addEventListener("click", handleWorkButton);
bankButtonElement.addEventListener("click", handleBankingButton);
repayLoanButtonElement.addEventListener("click", handleRepayLoanButton);
